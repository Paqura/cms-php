<?php

namespace ishop;

class Router
{
  public static $routes = [];
  public static $route = [];

  public static function add($regexp, $route = [])
  {
    self::$routes[$regexp] = $route;
  }

  public static function getRoutes()
  {
    return self::$routes;
  }

  public static function getRoute()
  {
    return self::$route;
  }

  public static function dispatch($url)
  {
    if (self::matchRoute($url)) {
      $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';
      if (class_exists($controller)) {
        $controllerObj = new $controller(self::$route);
        $action = self::lowerCamelCase(self::$route['action']) . 'Action';
        if (method_exists($controllerObj, $action)) {
          $controllerObj->$action();
        } else {
          throw new \Exception("Метод $controller::$action не найден", 404);
        }
      } else {
        throw new \Exception("Контроллер $controller не найден", 404);
      }
    } else {
      throw new \Exception('Страница не найдена', 404);
    }
  }

  protected static function upperCamelCase($name)
  {
    $name = str_replace('-', ' ', $name);
    $name = ucwords($name);
    $name = str_replace(' ', '', $name);
    return $name;
  }

  protected static function lowerCamelCase($name)
  {
    return lcfirst(self::upperCamelCase($name));
  }

  public static function matchRoute($url)
  {
    foreach (self::$routes as $pattern => $route) {
      if (preg_match("#{$pattern}#", $url, $matches)) {
        foreach ($matches as $k => $v) {
          if (is_string($k)) {
            $route[$k] = $v;
          }
        }
        if (empty($route['action'])) {
          $route['action'] = 'index';
        }
        if (!isset($rout['prefix'])) {
          $route['prefix'] = '';
        } else {
          $route['prefix'] .= '\\';
        }
        $route['controller'] = self::upperCamelCase($route['controller']);
        self::$route = $route;
        return true;
      }
    }
    return false;
  }
}